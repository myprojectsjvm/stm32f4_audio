/***************************************************************************//**
	@file		app.c
	@brief		Main application.
********************************************************************************
	Main application firmware.

	@note		Atollic TrueSTUDIO for STM32 Version 9.3.0.

	@note		STM32F4.

********************************************************************************
	@author		Jose Vinicius Melo.
	@date		July-2019.
*******************************************************************************/

#include "app.h"
#include "dma.h"
#include "i2s.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

#include "stm32f4xx_hal_i2s_comp.h"

/*******************************************************************************
	SETTINGS:
*******************************************************************************/

/*******************************************************************************
	DEFINES:
*******************************************************************************/

/* Button debounce in [ms]: */
#define APP_SW_DEBOUNCE_PERIOD	200

/* I2S: */
#define APP_I2S_BUFFER_SIZE_TX	256/2
#define APP_I2S_BUFFER_SIZE_RX	256/2

/*******************************************************************************
	TYPEDEFS:
*******************************************************************************/

/*******************************************************************************
	GLOBALS VARIABLES:
*******************************************************************************/

/* Button debounce: */
uint32_t AppSwDebounce;

/* UART: */
uint8_t AppUartDataRx;

/* I2S: */
uint16_t AppI2SBufferTx[2][APP_I2S_BUFFER_SIZE_TX];
uint16_t AppI2SBufferRx[2][APP_I2S_BUFFER_SIZE_RX];

/*******************************************************************************
	CONSTANTS:
*******************************************************************************/

/*******************************************************************************
	LOCAL PROTOTYPES:
*******************************************************************************/

/***************************************************************************//**
	@brief		Main application routine.
	@param		None.
	@return		None.
*******************************************************************************/

void AppMain(void) {

	/* Initializations: */
	HAL_TIM_Base_Start_IT(&htim1);
	HAL_UART_Receive_IT(&huart2, &AppUartDataRx, 1);

	for (uint32_t i = 0; i < APP_I2S_BUFFER_SIZE_TX; i++) {
		AppI2SBufferTx[0][i] = i;
	}
	for (uint32_t i = 0; i < APP_I2S_BUFFER_SIZE_TX; i++) {
		AppI2SBufferTx[1][i] = i + APP_I2S_BUFFER_SIZE_TX;
	}

	/* Main loop: */
	while (1) {

	}

}

/***************************************************************************//**
	@brief		External interrupt callback.
	@param		GPIO_Pin GPIO pin.
	@return		None;
*******************************************************************************/

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {

	static uint8_t sw = 0;

	if (!AppSwDebounce) {

		/* Debounce time: */
		AppSwDebounce = APP_SW_DEBOUNCE_PERIOD;

		/* LED blink: */
		HAL_GPIO_TogglePin(LD6_GPIO_Port, LD6_Pin);

		if (sw) {
			/* Stopping the I2S: */
			HAL_I2S_DMAStop(&hi2s3);
			sw = 0;
		}
		else {
			/* Transmitting and receiving: */
			HAL_I2SEx_TransmitReceive_DMA_DBM(&hi2s3, AppI2SBufferTx[0], AppI2SBufferTx[1], AppI2SBufferRx[0], AppI2SBufferRx[1], APP_I2S_BUFFER_SIZE_TX, APP_I2S_BUFFER_SIZE_RX);
			sw = 1;
		}

		/* LED blink: */
		HAL_GPIO_TogglePin(LD6_GPIO_Port, LD6_Pin);


	}
}

/***************************************************************************//**
	@brief		Timer interrupt callback.
	@param		htim Timer handle struct.
	@return		None;
*******************************************************************************/

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {

	static uint32_t t;

	/* Variables decrement: */
	if (AppSwDebounce) AppSwDebounce--;

	/* 5 second: */
	if (t < 1000) t++;
	else {
		t = 0;

		/* LED blink: */
		HAL_GPIO_TogglePin(LD3_GPIO_Port, LD3_Pin);

	}

}

/***************************************************************************//**
	@brief		UART interrupt callback.
	@param		htim UART handle struct.
	@return		None;
*******************************************************************************/

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {

	HAL_UART_Receive_IT(&huart2, &AppUartDataRx, 1);

}

//----------------------------------------------------------------------------//
