/***************************************************************************//**
	@file		stm32f4xx_hal_i2s_comp.h
	@brief		Complementary I2S HAL module driver.
********************************************************************************
	Complementary I2S HAL module driver.

	This driver is complementary with I2S routines to enable DMA in double mode.

	@note		Atollic TrueSTUDIO for STM32 Version 9.3.0.

	@note		STM32F4 microcontrollers.

********************************************************************************
	@author		Jose Vinicius Melo.
	@date		August-2019.
*******************************************************************************/

#include "stm32f4xx_hal_i2s_comp.h"

/*******************************************************************************
	SETTINGS:
*******************************************************************************/

/*******************************************************************************
	DEFINES:
*******************************************************************************/

/*******************************************************************************
	TYPEDEFS:
*******************************************************************************/

/*******************************************************************************
	GLOBALS VARIABLES:
*******************************************************************************/

/*******************************************************************************
	CONSTANTS:
*******************************************************************************/

/*******************************************************************************
	LOCAL PROTOTYPES:
*******************************************************************************/

static void I2S_DMATxCplt_DBM_0(DMA_HandleTypeDef *hdma);
static void I2S_DMATxCplt_DBM_1(DMA_HandleTypeDef *hdma);
static void I2S_DMARxCplt_DBM_0(DMA_HandleTypeDef *hdma);
static void I2S_DMARxCplt_DBM_1(DMA_HandleTypeDef *hdma);
static void I2S_DMAError_DBM(DMA_HandleTypeDef *hdma);
static void I2SEx_DMAError_DBM(DMA_HandleTypeDef *hdma);

/***************************************************************************//**
	@brief		DMA I2S transmit process complete callback (M0).
	@param  	hdma pointer to a DMA_HandleTypeDef structure that contains
				the configuration information for the specified DMA module.
	@return		None.
*******************************************************************************/
static void I2S_DMATxCplt_DBM_0(DMA_HandleTypeDef *hdma)
{

	I2S_HandleTypeDef *hi2s = (I2S_HandleTypeDef *)((DMA_HandleTypeDef *)hdma)->Parent;

	HAL_I2S_TxCpltCallback_DBM(hi2s, 0);

}

/***************************************************************************//**
	@brief		DMA I2S transmit process complete callback (M1).
	@param  	hdma pointer to a DMA_HandleTypeDef structure that contains
				the configuration information for the specified DMA module.
	@return		None.
*******************************************************************************/
static void I2S_DMATxCplt_DBM_1(DMA_HandleTypeDef *hdma)
{

	I2S_HandleTypeDef *hi2s = (I2S_HandleTypeDef *)((DMA_HandleTypeDef *)hdma)->Parent;

	HAL_I2S_TxCpltCallback_DBM(hi2s, 1);

}

/***************************************************************************//**
	@brief		DMA I2S receive process complete callback (M0).
	@param  	hdma pointer to a DMA_HandleTypeDef structure that contains
				the configuration information for the specified DMA module.
	@return		None.
*******************************************************************************/

static void I2S_DMARxCplt_DBM_0(DMA_HandleTypeDef *hdma)
{

	I2S_HandleTypeDef *hi2s = (I2S_HandleTypeDef *)((DMA_HandleTypeDef *)hdma)->Parent;

	HAL_I2S_RxCpltCallback_DBM(hi2s, 0);

}

/***************************************************************************//**
	@brief		DMA I2S receive process complete callback (M1).
	@param  	hdma pointer to a DMA_HandleTypeDef structure that contains
				the configuration information for the specified DMA module.
	@return		None.
*******************************************************************************/

static void I2S_DMARxCplt_DBM_1(DMA_HandleTypeDef *hdma)
{

	I2S_HandleTypeDef *hi2s = (I2S_HandleTypeDef *)((DMA_HandleTypeDef *)hdma)->Parent;

	HAL_I2S_RxCpltCallback_DBM(hi2s, 1);

}

/***************************************************************************//**
	@brief		DMA I2S communication error callback.
	@param  	hdma pointer to a DMA_HandleTypeDef structure that contains
				the configuration information for the specified DMA module.
	@return		None.
*******************************************************************************/

static void I2S_DMAError_DBM(DMA_HandleTypeDef *hdma)
{
	I2S_HandleTypeDef *hi2s = (I2S_HandleTypeDef *)((DMA_HandleTypeDef *)hdma)->Parent;

	/* Disable Rx and Tx DMA Request */
	CLEAR_BIT(hi2s->Instance->CR2, (SPI_CR2_RXDMAEN | SPI_CR2_TXDMAEN));
	hi2s->TxXferCount = 0U;
	hi2s->RxXferCount = 0U;

	hi2s->State = HAL_I2S_STATE_READY;

	/* Set the error code and execute error callback*/
	SET_BIT(hi2s->ErrorCode, HAL_I2S_ERROR_DMA);

	/* Call user error callback */
	HAL_I2S_ErrorCallback(hi2s);

}

/***************************************************************************//**
	@brief		DMA I2S communication error callback
	@param  	hdma pointer to a DMA_HandleTypeDef structure that contains
				the configuration information for the specified DMA module.
	@return		None.
*******************************************************************************/

static void I2SEx_DMAError_DBM(DMA_HandleTypeDef *hdma)
{

	I2S_HandleTypeDef *hi2s = (I2S_HandleTypeDef *)((DMA_HandleTypeDef *)hdma)->Parent;

	/* Disable Rx and Tx DMA Request */
	CLEAR_BIT(hi2s->Instance->CR2, (SPI_CR2_RXDMAEN | SPI_CR2_TXDMAEN));
	CLEAR_BIT(I2SxEXT(hi2s->Instance)->CR2, (SPI_CR2_RXDMAEN | SPI_CR2_TXDMAEN));

	hi2s->TxXferCount = 0U;
	hi2s->RxXferCount = 0U;

	hi2s->State = HAL_I2S_STATE_READY;

	/* Set the error code and execute error callback*/
	SET_BIT(hi2s->ErrorCode, HAL_I2S_ERROR_DMA);

	/* Call user error callback */
	HAL_I2S_ErrorCallback(hi2s);

}

/***************************************************************************//**
	@brief		Transmit an amount of data in non-blocking mode with DMA in
				double buffer mode.
	@param		hi2s 	pointer to a I2S_HandleTypeDef structure that contains
						the configuration information for I2S module.
	@param 		pData0	first 16-bit pointer to the Transmit data buffer.
	@param 		pData1	second 16-bit pointer to the Transmit data buffer.
	@param 		Size	Size number of data sample to be sent:
	@note		When a 16-bit data frame or a 16-bit data frame extended is
				selected during the I2S configuration phase, the Size parameter
				means the number of 16-bit data length in the transaction and
				when a 24-bit data frame or a 32-bit data frame is selected the
				Size parameter means the number of 16-bit data length.
	@note		The I2S is kept enabled at the end of transaction to avoid the
				clock de-synchronization between Master and Slave
				(example: audio streaming).
	@return		HAL status.
*******************************************************************************/

HAL_StatusTypeDef HAL_I2S_Transmit_DMA_DBM(I2S_HandleTypeDef *hi2s, uint16_t *pData0, uint16_t *pData1, uint16_t Size)
{

	uint32_t tmpreg_cfgr;

	if ((pData0 == NULL) || (pData1 == NULL) || (Size == 0U))
	{
		return  HAL_ERROR;
	}

	/* Process Locked */
	__HAL_LOCK(hi2s);

	if (hi2s->State != HAL_I2S_STATE_READY)
	{
		__HAL_UNLOCK(hi2s);
		return HAL_BUSY;
	}
	/* Set state and reset error code */
	hi2s->State = HAL_I2S_STATE_BUSY_TX;
	hi2s->ErrorCode = HAL_I2S_ERROR_NONE;
	hi2s->pTxBuffPtr = pData0;

	tmpreg_cfgr = hi2s->Instance->I2SCFGR & (SPI_I2SCFGR_DATLEN | SPI_I2SCFGR_CHLEN);

	if ((tmpreg_cfgr == I2S_DATAFORMAT_24B) || (tmpreg_cfgr == I2S_DATAFORMAT_32B))
	{
		hi2s->TxXferSize = (Size << 1U);
		hi2s->TxXferCount = (Size << 1U);
	}
	else
	{
		hi2s->TxXferSize = Size;
		hi2s->TxXferCount = Size;
	}

	/* Set the I2S Tx DMA Half transfer complete callback */
	hi2s->hdmatx->XferHalfCpltCallback = NULL;

	/* Set the I2S Tx DMA transfer complete callback */
	hi2s->hdmatx->XferCpltCallback = I2S_DMATxCplt_DBM_0;

	/* Set the I2S Tx DMA transfer complete callback */
	hi2s->hdmatx->XferM1CpltCallback = I2S_DMATxCplt_DBM_1;

	/* Set the DMA error callback */
	hi2s->hdmatx->XferErrorCallback = I2S_DMAError_DBM;

	/* Enable the Tx DMA Stream/Channel */
	hi2s->hdmatx->Instance->CR &= ~(uint32_t)DMA_SxCR_CT;	/* Current target memory is Memory 0. */
	if (HAL_OK != HAL_DMAEx_MultiBufferStart_IT(hi2s->hdmatx, (uint32_t)hi2s->pTxBuffPtr, (uint32_t)&hi2s->Instance->DR, (uint32_t)pData1, hi2s->TxXferSize))
	{
		/* Update SPI error code */
		SET_BIT(hi2s->ErrorCode, HAL_I2S_ERROR_DMA);
		hi2s->State = HAL_I2S_STATE_READY;

		__HAL_UNLOCK(hi2s);
		return HAL_ERROR;
	}

	/* Check if the I2S is already enabled */
	if (HAL_IS_BIT_CLR(hi2s->Instance->I2SCFGR, SPI_I2SCFGR_I2SE))
	{
		/* Enable I2S peripheral */
		__HAL_I2S_ENABLE(hi2s);
	}

	/* Check if the I2S Tx request is already enabled */
	if (HAL_IS_BIT_CLR(hi2s->Instance->CR2, SPI_CR2_TXDMAEN))
	{
		/* Enable Tx DMA Request */
		SET_BIT(hi2s->Instance->CR2, SPI_CR2_TXDMAEN);
	}

	__HAL_UNLOCK(hi2s);
	return HAL_OK;

}

/***************************************************************************//**
	@brief		Receive an amount of data in non-blocking mode with DMA in
				double buffer mode.
	@param		hi2s 	pointer to a I2S_HandleTypeDef structure that contains
						the configuration information for I2S module.
	@param 		pData0	first 16-bit pointer to the Receive data buffer.
	@param 		pData1	second 16-bit pointer to the Receive data buffer.
	@param 		Size	Size number of data sample to be sent:
	@note		When a 16-bit data frame or a 16-bit data frame extended is
				selected during the I2S configuration phase, the Size parameter
				means the number of 16-bit data length in the transaction and
				when a 24-bit data frame or a 32-bit data frame is selected the
				Size parameter means the number of 16-bit data length.
	@note		The I2S is kept enabled at the end of transaction to avoid the
				clock de-synchronization between Master and Slave
				(example: audio streaming).
	@return		HAL status.
*******************************************************************************/

HAL_StatusTypeDef HAL_I2S_Receive_DMA_DBM(I2S_HandleTypeDef *hi2s, uint16_t *pData0, uint16_t *pData1, uint16_t Size)
{
	uint32_t tmpreg_cfgr;

	if ((pData0 == NULL) || (pData1 == NULL) || (Size == 0U))
	{
		return  HAL_ERROR;
	}

	/* Process Locked */
	__HAL_LOCK(hi2s);

	if (hi2s->State != HAL_I2S_STATE_READY)
	{
		__HAL_UNLOCK(hi2s);
		return HAL_BUSY;
	}

	/* Set state and reset error code */
	hi2s->State = HAL_I2S_STATE_BUSY_RX;
	hi2s->ErrorCode = HAL_I2S_ERROR_NONE;
	hi2s->pRxBuffPtr = pData0;

	tmpreg_cfgr = hi2s->Instance->I2SCFGR & (SPI_I2SCFGR_DATLEN | SPI_I2SCFGR_CHLEN);

	if ((tmpreg_cfgr == I2S_DATAFORMAT_24B) || (tmpreg_cfgr == I2S_DATAFORMAT_32B))
	{
		hi2s->RxXferSize = (Size << 1U);
		hi2s->RxXferCount = (Size << 1U);
	}
	else
	{
		hi2s->RxXferSize = Size;
		hi2s->RxXferCount = Size;
	}

	/* Set the I2S Rx DMA Half transfer complete callback */
	hi2s->hdmarx->XferHalfCpltCallback = NULL;

	/* Set the I2S Rx DMA transfer complete callback */
	hi2s->hdmarx->XferCpltCallback = I2S_DMARxCplt_DBM_0;

	/* Set the I2S Rx DMA transfer complete callback */
	hi2s->hdmarx->XferM1CpltCallback = I2S_DMARxCplt_DBM_1;

	/* Set the DMA error callback */
	hi2s->hdmarx->XferErrorCallback = I2S_DMAError_DBM;

	/* Check if Master Receiver mode is selected */
	if ((hi2s->Instance->I2SCFGR & SPI_I2SCFGR_I2SCFG) == I2S_MODE_MASTER_RX)
	{
		/* Clear the Overrun Flag by a read operation to the SPI_DR register followed by a read
		access to the SPI_SR register. */
		__HAL_I2S_CLEAR_OVRFLAG(hi2s);
	}

	/* Enable the Rx DMA Stream/Channel */
	hi2s->hdmarx->Instance->CR &= ~(uint32_t)DMA_SxCR_CT;	/* Current target memory is Memory 0. */
	if (HAL_OK != HAL_DMAEx_MultiBufferStart_IT(hi2s->hdmarx, (uint32_t)&hi2s->Instance->DR, (uint32_t)hi2s->pRxBuffPtr, (uint32_t)pData1, hi2s->RxXferSize))
	{
		/* Update SPI error code */
		SET_BIT(hi2s->ErrorCode, HAL_I2S_ERROR_DMA);
		hi2s->State = HAL_I2S_STATE_READY;

		__HAL_UNLOCK(hi2s);
		return HAL_ERROR;
	}

	/* Check if the I2S is already enabled */
	if (HAL_IS_BIT_CLR(hi2s->Instance->I2SCFGR, SPI_I2SCFGR_I2SE))
	{
		/* Enable I2S peripheral */
		__HAL_I2S_ENABLE(hi2s);
	}

	/* Check if the I2S Rx request is already enabled */
	if (HAL_IS_BIT_CLR(hi2s->Instance->CR2, SPI_CR2_RXDMAEN))
	{
		/* Enable Rx DMA Request */
		SET_BIT(hi2s->Instance->CR2, SPI_CR2_RXDMAEN);
	}

	__HAL_UNLOCK(hi2s);
	return HAL_OK;

}

/***************************************************************************//**
	@brief		Full-Duplex Transmit/Receive data in non-blocking mode using DMA
				in double buffer mode.
	@param		hi2s pointer to a I2S_HandleTypeDef structure that contains
				the configuration information for I2S module.
	@param		pTxData0 first 16-bit pointer to the Transmit data buffer.
	@param		pTxData1 second 16-bit pointer to the Transmit data buffer.
	@param		pRxData0 first 16-bit pointer to the Receive data buffer.
	@param		pRxData1 second 16-bit pointer to the Receive data buffer.
	@param 		txSize	Size number of data sample to be sent (Transmit).
	@param 		rxSize	Size number of data sample to be sent (Receive).
	@note		When a 16-bit data frame or a 16-bit data frame extended is
				selected during the I2S configuration phase, the Size parameter
				means the number of 16-bit data length in the transaction and
				when a 24-bit data frame or a 32-bit data frame is selected the
				Size parameter means the number of 16-bit data length.
	@note		The I2S is kept enabled at the end of transaction to avoid the
				clock de-synchronization between Master and Slave
				(example: audio streaming).
	@return		HAL status.
*******************************************************************************/

HAL_StatusTypeDef HAL_I2SEx_TransmitReceive_DMA_DBM(	I2S_HandleTypeDef *hi2s,
														uint16_t *pTxData0,
														uint16_t *pTxData1,
														uint16_t *pRxData0,
														uint16_t *pRxData1,
														uint16_t txSize,
														uint16_t rxSize)
{

	uint32_t *tmp = NULL;
	uint32_t tmp1 = 0U;

	if (hi2s->State != HAL_I2S_STATE_READY)
	{
		return HAL_BUSY;
	}

	if ((pTxData0 == NULL) || (pTxData1 == NULL) || (pRxData0 == NULL) || (pRxData1 == NULL) || (txSize == 0U) || (rxSize == 0U))
	{
		return  HAL_ERROR;
	}

	/* Process Locked */
	__HAL_LOCK(hi2s);

	hi2s->pTxBuffPtr = pTxData0;
	hi2s->pRxBuffPtr = pRxData0;

	tmp1 = hi2s->Instance->I2SCFGR & (SPI_I2SCFGR_DATLEN | SPI_I2SCFGR_CHLEN);
	/* Check the Data format: When a 16-bit data frame or a 16-bit data frame extended
	is selected during the I2S configuration phase, the Size parameter means the number
	of 16-bit data length in the transaction and when a 24-bit data frame or a 32-bit data
	frame is selected the Size parameter means the number of 16-bit data length. */
	if ((tmp1 == I2S_DATAFORMAT_24B) || (tmp1 == I2S_DATAFORMAT_32B))
	{
		hi2s->TxXferSize  = (txSize << 1U);
		hi2s->TxXferCount = (txSize << 1U);
		hi2s->RxXferSize  = (rxSize << 1U);
		hi2s->RxXferCount = (rxSize << 1U);
	}
	else
	{
		hi2s->TxXferSize  = txSize;
		hi2s->TxXferCount = txSize;
		hi2s->RxXferSize  = rxSize;
		hi2s->RxXferCount = rxSize;
	}

	hi2s->ErrorCode = HAL_I2S_ERROR_NONE;
	hi2s->State     = HAL_I2S_STATE_BUSY_TX_RX;

	/* Set the I2S Rx DMA Half transfer complete callback */
	hi2s->hdmarx->XferHalfCpltCallback = NULL;

	/* Set the I2S Rx DMA transfer complete callback */
	hi2s->hdmarx->XferCpltCallback  = I2S_DMARxCplt_DBM_0;

	/* Set the I2S Rx DMA transfer complete callback */
	hi2s->hdmarx->XferM1CpltCallback  = I2S_DMARxCplt_DBM_1;

	/* Set the I2S Rx DMA error callback */
	hi2s->hdmarx->XferErrorCallback = I2SEx_DMAError_DBM;

	/* Set the I2S Tx DMA Half transfer complete callback */
	hi2s->hdmatx->XferHalfCpltCallback  = NULL;

	/* Set the I2S Tx DMA transfer complete callback */
	hi2s->hdmatx->XferCpltCallback  = I2S_DMATxCplt_DBM_0;

	/* Set the I2S Tx DMA transfer complete callback */
	hi2s->hdmatx->XferM1CpltCallback  = I2S_DMATxCplt_DBM_1;

	/* Set the I2S Tx DMA error callback */
	hi2s->hdmatx->XferErrorCallback = I2SEx_DMAError_DBM;

	tmp1 = hi2s->Instance->I2SCFGR & SPI_I2SCFGR_I2SCFG;

	/* Check if the I2S_MODE_MASTER_TX or I2S_MODE_SLAVE_TX Mode is selected */
	if ((tmp1 == I2S_MODE_MASTER_TX) || (tmp1 == I2S_MODE_SLAVE_TX))
	{
		/* Enable the Rx DMA Stream */
		tmp = (uint32_t *)&pRxData0;
		hi2s->hdmarx->Instance->CR &= ~(uint32_t)DMA_SxCR_CT;	/* Current target memory is Memory 0. */
		HAL_DMAEx_MultiBufferStart_IT(hi2s->hdmarx, (uint32_t)&I2SxEXT(hi2s->Instance)->DR, *(uint32_t *)tmp, (uint32_t)pRxData1 , hi2s->RxXferSize);

		/* Enable Rx DMA Request */
		SET_BIT(I2SxEXT(hi2s->Instance)->CR2, SPI_CR2_RXDMAEN);

		/* Enable the Tx DMA Stream */
		tmp = (uint32_t *)&pTxData0;
		hi2s->hdmatx->Instance->CR &= ~(uint32_t)DMA_SxCR_CT;	/* Current target memory is Memory 0. */
		HAL_DMAEx_MultiBufferStart_IT(hi2s->hdmatx, *(uint32_t *)tmp, (uint32_t)&hi2s->Instance->DR, (uint32_t)pTxData1, hi2s->TxXferSize);

		/* Enable Tx DMA Request */
		SET_BIT(hi2s->Instance->CR2, SPI_CR2_TXDMAEN);

		/* Check if the I2S is already enabled */
		if ((hi2s->Instance->I2SCFGR & SPI_I2SCFGR_I2SE) != SPI_I2SCFGR_I2SE)
		{
			/* Enable I2Sext(receiver) before enabling I2Sx peripheral */
			__HAL_I2SEXT_ENABLE(hi2s);

			/* Enable I2S peripheral after the I2Sext */
			__HAL_I2S_ENABLE(hi2s);
		}
	}
	else
	{
		/* Check if Master Receiver mode is selected */
		if ((hi2s->Instance->I2SCFGR & SPI_I2SCFGR_I2SCFG) == I2S_MODE_MASTER_RX)
		{
			/* Clear the Overrun Flag by a read operation on the SPI_DR register followed by a read
			access to the SPI_SR register. */
			__HAL_I2S_CLEAR_OVRFLAG(hi2s);
		}

		/* Enable the Tx DMA Stream */
		tmp = (uint32_t *)&pTxData0;
		hi2s->hdmatx->Instance->CR &= ~(uint32_t)DMA_SxCR_CT;	/* Current target memory is Memory 0. */
		HAL_DMAEx_MultiBufferStart_IT(hi2s->hdmatx, *(uint32_t *)tmp, (uint32_t)&I2SxEXT(hi2s->Instance)->DR, (uint32_t)pTxData1, hi2s->TxXferSize);

		/* Enable Tx DMA Request */
		SET_BIT(I2SxEXT(hi2s->Instance)->CR2, SPI_CR2_TXDMAEN);

		/* Enable the Rx DMA Stream */
		tmp = (uint32_t *)&pRxData0;
		hi2s->hdmarx->Instance->CR &= ~(uint32_t)DMA_SxCR_CT;	/* Current target memory is Memory 0. */
		HAL_DMAEx_MultiBufferStart_IT(hi2s->hdmarx, (uint32_t)&hi2s->Instance->DR, *(uint32_t *)tmp, (uint32_t)pRxData1, hi2s->RxXferSize);

		/* Enable Rx DMA Request */
		SET_BIT(hi2s->Instance->CR2, SPI_CR2_RXDMAEN);

		/* Check if the I2S is already enabled */
		if ((hi2s->Instance->I2SCFGR & SPI_I2SCFGR_I2SE) != SPI_I2SCFGR_I2SE)
		{
			/* Enable I2Sext(transmitter) before enabling I2Sx peripheral */
			__HAL_I2SEXT_ENABLE(hi2s);
			/* Enable I2S peripheral before the I2Sext */
			__HAL_I2S_ENABLE(hi2s);
		}
	}

	__HAL_UNLOCK(hi2s);
	return HAL_OK;

}

/***************************************************************************//**
	@brief		I2S transmission callback.
	@param		hi2s	I2S peripheral structure.
	@param		index 	Memory index.
	@return		None.
*******************************************************************************/

void HAL_I2S_TxCpltCallback_DBM(I2S_HandleTypeDef *hi2s, const uint32_t index) {

}

/***************************************************************************//**
	@brief		I2S reception callback.
	@param		hi2s	I2S peripheral structure.
	@param		index 	Memory index.
	@return		None.
*******************************************************************************/

void HAL_I2S_RxCpltCallback_DBM(I2S_HandleTypeDef *hi2s, const uint32_t index) {

}

//----------------------------------------------------------------------------//
