/***************************************************************************//**
	@file		stm32f4xx_hal_i2s_comp.h
	@brief		Complementary I2S HAL module driver.
********************************************************************************
	Complementary I2S HAL module driver.

	This driver is complementary with I2S routines to enable DMA in double mode.

	@note		Atollic TrueSTUDIO for STM32 Version 9.3.0.

	@note		STM32F4 microcontrollers.

********************************************************************************
	@author		Jose Vinicius Melo.
	@date		August-2019.
*******************************************************************************/

#ifndef STM32F4XX_HAL_I2S_COMP_H
#define STM32F4XX_HAL_I2S_COMP_H

#include "stm32f4xx.h"

/*******************************************************************************
	SETTINGS:
*******************************************************************************/

/*******************************************************************************
	DEFINES:
*******************************************************************************/

/*******************************************************************************
	TYPEDEFS:
*******************************************************************************/

/*******************************************************************************
	EXTERNAL VARIABLES:
*******************************************************************************/

/*******************************************************************************
	CONSTANTS:
*******************************************************************************/

/*******************************************************************************
	PROTOTYPES:
*******************************************************************************/

HAL_StatusTypeDef HAL_I2S_Transmit_DMA_DBM(I2S_HandleTypeDef *hi2s, uint16_t *pData0, uint16_t *pData1, uint16_t Size);
HAL_StatusTypeDef HAL_I2S_Receive_DMA_DBM(I2S_HandleTypeDef *hi2s, uint16_t *pData0, uint16_t *pData1, uint16_t Size);
HAL_StatusTypeDef HAL_I2SEx_TransmitReceive_DMA_DBM( I2S_HandleTypeDef *hi2s, uint16_t *pTxData0, uint16_t *pTxData1, uint16_t *pRxData0, uint16_t *pRxData1, uint16_t txSize,uint16_t rxSize);

void HAL_I2S_TxCpltCallback_DBM(I2S_HandleTypeDef *hi2s, const uint32_t index);
void HAL_I2S_RxCpltCallback_DBM(I2S_HandleTypeDef *hi2s, const uint32_t index);

//----------------------------------------------------------------------------//

#endif /* STM32F4XX_HAL_I2S_COMP_H */

//----------------------------------------------------------------------------//
